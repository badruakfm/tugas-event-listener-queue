<?php

namespace App\Listeners;

use App\Events\BlogCreated;
use App\Mail\AkanDireviewMail;
use App\Mail\MintaDireviewMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class BlogCreatedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogCreated  $event
     * @return void
     */
    public function handle(BlogCreated $event)
    {
        Mail::to($event->user)->send(new AkanDireviewMail($event->user, $event->blog));
        Mail::to('editor@example.com')->send(new MintaDireviewMail($event->user, $event->blog));
    }
}
