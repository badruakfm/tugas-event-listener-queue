<?php

namespace App\Listeners;

use App\Events\BlogPublished;
use App\Mail\sudahDipublishMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class BlogPublishedNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublished  $event
     * @return void
     */
    public function handle(BlogPublished $event)
    {
        Mail::to($event->user)->send(new sudahDipublishMail($event->user, $event->blog));
    }
}
