<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Events\BlogCreated;
use App\Events\BlogPublished;
use App\Mail\AkanDireviewMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BlogController extends Controller
{
    public function buatBlog(Request $request)
    {
        $user = User::find($request->user_id);

        $blog = Blog::create($request->all());


        event(new BlogCreated($user, $blog));

        return response()->json([
            'message' => 'Blog disimpan'
        ]);
    }

    public function publishBlog(Request $request)
    {
        $blog = Blog::find($request->id);

        $user = User::where('id', $blog->user_id)->first();

        Blog::where('id',$request->id)->update([
            'publish_status' => '1'
        ]);

        event(new BlogPublished($user, $blog));

        return response()->json([
            'message' => 'Blog dipublish'
        ]);
    }
}
