<?php

namespace App\Mail;

use App\Blog;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sudahDipublishMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $blog;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Blog $blog)
    {
        $this->user = $user;
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->view('sudah_dipublish')
            ->with([
                'name' => $this->user->name,
                'judul' => $this->blog->name
            ]);
    }
}
